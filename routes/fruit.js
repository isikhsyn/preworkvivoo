const express = require('express');
const router = express.Router();

/* GET users listing. */
const Fruit = require('../models/fruit');

router.get('/', (req, res) =>{
   const promise = Fruit.find({

   }) ;
   promise.then((data) => {
       res.json(data);
   }).catch((err)=> {
       res.json(err);
   })
});

router.get('/:fruitid',(req, res, next) =>{
    const promise = Fruit.findById(req.params.fruitid);
    promise.then((fruit) => {
        if (!fruit)
            next('Meyve bulunamadi.');
        res.json(fruit);
    }).catch((err)=> {
        res.json(err);
    })
});

router.put('/:fruitid',(req, res, next) =>{
    // noinspection JSAnnotator
    const promise = Fruit.findByIdAndUpdate(
        req.params.fruitid,
        req.body,
        {
            new: true
        }
    );
    promise.then((fruit) => {
        if (!fruit)
            next({message: 'Meyve bulunamadi.'});
        res.json(fruit);
    }).catch((err)=> {
        res.json(err);
    });
});

router.delete('/:fruitid',(req, res, next) =>{
    const promise = Fruit.findByIdAndRemove(req.params.fruitid);
    promise.then((fruit) => {
        if (!fruit)
            next('Meyve bulunamadi.');
        res.json({status: 'Silindi.'});
    }).catch((err)=> {
        res.json(err);
    })
});


router.post('/', (req, res, next) => {
  const {adi, meyveid} =req.body;
  const fruit = new Fruit({
      adi: adi,
      meyveid: meyveid,

  });

 const promise = fruit.save();
 promise.then((data)=> {
     res.json ({status: 'Ok'});
 }).catch((err)=> {
     res.json(err);
 })

});

module.exports = router;
